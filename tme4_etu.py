from arftools import *
import pdb;
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

def decorator_vec(fonc):
    def vecfonc(datax,datay,w,*args,**kwargs):
        if not hasattr(datay,"__len__"):
            datay = np.array([datay])
        datax,datay,w =  datax.reshape(len(datay),-1),datay.reshape(-1,1),w.reshape((1,-1))
        return fonc(datax,datay,w,*args,**kwargs)
    return vecfonc

@decorator_vec
def mse(datax,datay,w):
    """ retourne la moyenne de l'erreur aux moindres carres """
    return np.mean((datax.dot(w.T) - datay)**2)

@decorator_vec
def mse_g(datax,datay,w):
    """ retourne le gradient moyen de l'erreur au moindres carres """
    return 2*(datax.dot(w.T) - datay).T.dot(datax)/len(datay)

@decorator_vec
def hinge(datax,datay,w):
    """ retourne la moyenne de l'erreur hinge """
    return np.mean(np.maximum(0, - datay * datax.dot(w.T)))

@decorator_vec
def hinge_g(datax,datay,w):
    """ retourne le gradient moyen de l'erreur hinge """
    coeff_maj_exemples = - datay * np.sign((- datay * datax.dot(w.T)).clip(min=0))
    # vaut 0 quand l'exemple est correctement classé, -yi sinon.
    return coeff_maj_exemples.T.dot(datax)/len(datay)

class Lineaire(object):
    def __init__(self,loss=hinge,loss_g=hinge_g,max_iter=1000,eps=0.01):
        """ :loss: fonction de cout
            :loss_g: gradient de la fonction de cout
            :max_iter: nombre d'iterations
            :eps: pas de gradient
        """
        self.max_iter, self.eps = max_iter, eps
        self.loss, self.loss_g = loss, loss_g

    def fit(self,datax,datay,testx=None,testy=None, batchSize=-1):
        """ :datax: donnees de train
            :datay: label de train
            :testx: donnees de test
            :testy: label de test
        """
        N = len(datay)
        datay = datay.reshape(-1,1)
        datax = datax.reshape(N,-1)
        # Shuffle the data to make sure its homegenous
        shuffled = list(zip(datax, datay))
        np.random.shuffle(shuffled)
        datax, datay = zip(*shuffled)
        datax, datay = np.array(datax), np.array(datay)
        
        # Initialize weights with random variables
        self.w = np.random.random((1, datax.shape[1]))
        
        batchSize = N if batchSize == -1 else batchSize

        w_histo, loss_histo, grad_histo = np.array(self.w), [], []
        for i in range(0, self.max_iter):
            w_histo = np.concatenate((w_histo, self.w))
            loss_histo.append(self.loss(datax, datay, self.w))
            grad_histo.append(self.loss_g(datax, datay, self.w))
            
            self.w = self.w - self.eps * self.loss_g(datax, datay, self.w)
        return w_histo, loss_histo, grad_histo

    def predict(self,datax):
        if len(datax.shape)==1:
            datax = datax.reshape(1,-1)
        return datax.dot(self.w.T)

    def score(self,datax,datay):
        return self.loss(datax, datay, self.w)



def load_usps(fn):
    with open(fn,"r") as f:
        f.readline()
        data = [[float(x) for x in l.split()] for l in f if len(l.split())>2]
    tmp=np.array(data)
    return tmp[:,1:],tmp[:,0].astype(int)

def show_usps(data):
    plt.imshow(data.reshape((16,16)),interpolation="nearest",cmap="gray")



def plot_error(datax,datay,f,step=10):
    grid,x1list,x2list=make_grid(xmin=-4,xmax=4,ymin=-4,ymax=4)
    plt.contourf(x1list,x2list,np.array([f(datax,datay,w) for w in grid]).reshape(x1list.shape),25)
    plt.colorbar()
    plt.show()



if __name__=="__main__":
    """ Tracer des isocourbes de l'erreur """
    plt.ion()
    trainx,trainy =  gen_arti(nbex=1000,data_type=0,epsilon=1)
    testx,testy =  gen_arti(nbex=1000,data_type=0,epsilon=1)
    plt.figure()
    plot_error(trainx,trainy,mse)
    plt.figure()
    plot_error(trainx,trainy,hinge)
    perceptron = Lineaire(hinge,hinge_g,max_iter=1000,eps=0.1)
    perceptron.fit(trainx,trainy)
    print("Erreur : train %f, test %f"% (perceptron.score(trainx,trainy),perceptron.score(testx,testy)))
    plt.figure()
    plot_frontiere(trainx,perceptron.predict,200)
    plot_data(trainx,trainy)

 